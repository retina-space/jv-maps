<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>jVectorMap</title>
	<link rel="stylesheet" href="css/jquery-jvectormap-1.2.2.css" type="text/css" media="screen"/>
	<link rel="stylesheet" href="css/site.css" type="text/css" media="screen"/>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="js/customMap.js"></script>
	<script src="maps/world_map_sl.js"></script>
</head>
<body>
	<div id="worldMap"></div>
	<script>
	$(function(){
		$('#worldMap').vectorMap({
			map: 'world_mill_sl',
			onRegionLabelShow: function(e, el, code){
				el.html(el.html() +' (GDP - '+gdpData[code]+')');
			}
		});
	});
	</script>
</body>
</html>